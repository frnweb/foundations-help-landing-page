<?php
/**
 * Template Name: Formatting Page
 * Description: This template generates all markup for shortcodes and typography styles.
 */

$context = Timber::get_context();
$post = new Timber\Post();

Timber::render( array( 'templates/formatting.twig', 'page.twig' ), $context );
