<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];


$card_list = new FieldsBuilder('card_list');

$card_list
	->addGroup('card_list')

		// Header
		->addText('header', [
			'label' => 'Header',
			'ui' => $config->ui
		])
			->setInstructions('Header for the card')
		//List Class
		->addTrueFalse('class_field', [
			'label' => 'List Class',
			'wrapper' => ['width' => 10]
			])
			->addText('list_class', [
				'wrapper' => ['width' => 20]
				])
			->conditional('class_field', '==', 1 )	

		// List
		->addRepeater('list_repeater', [
			'label' => 'List Items',
			'min' => 1,
			'max' => 8,
			'button_label' => 'Add List item',
			'layout' => 'block',
			])
	  	->addText('list_item', [
	  		'label' => 'List Item Text'
	  	])
	  	//Make items links
		->addTrueFalse('add_link', [
			'label' => 'Add Link?',
			'wrapper' => ['width' => 15]
			])
	  		->addText('item_url', [
		  		'label' => 'Item URL',
		  		'wrapper' => ['width' => 85]
		  	])
	  		->conditional('add_link', '==', 1 )		
	  ->endRepeater()
  ->endGroup();

return $card_list;