<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$location = new FieldsBuilder('location');

$location
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'))
		->addFields(get_field_partial('partials.grid_options'));

$location

	->addTab('content', ['placement' => 'left'])

		//Header
		->addText('header', [
			'label' => 'Location Header'
	    	])
	    	->setInstructions('This is optional')

	    //Facility Description
		->addWysiwyg('callout', [
			'label' => 'Callout Text'
	    	])
	    	->setInstructions('This will appear at the very bottom of the module')

		//Repeater
		->addRepeater('locations', [
		  'min' => 1,
		  'max' => 12,
		  'button_label' => 'Add Location',
		  'layout' => 'block',
		  'wrapper' => [
	          'class' => 'deck',
	        ],
		])

		//Image 
		->addImage('image')

		//City 
		->addText('city')

		//State 
		->addText('state')

		//Facility Name 
		->addText('facility', [
			'label' => 'Facility Name'
	    	])

		//Google Map
        ->addGoogleMap('google_map', [
			'label' => 'Google Map Location'
	    	])

        //Facility Description
		->addWysiwyg('description', [
			'label' => 'Facility Description'
	    	])
	    	->setInstructions('Add small unordered list of facility information')

	    //Link
	    ->addLink('button')
	   	->endRepeated;
		
    
return $location;