<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
    'ui' => 1,
    'wrapper' => ['width' => 50],
];

$wildcard = new FieldsBuilder('wildcard');

$wildcard
    ->addTab('settings', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.add_class'))
        ->addFields(get_field_partial('partials.module_title'));


$wildcard
    ->addTab('content', ['placement' => 'left'])
    //Repeater
    ->addRepeater('deck', [
      'min' => 1,
      'max' => 12,
      'button_label' => 'Add Card',
      'layout' => 'block',
      'wrapper' => [
          'class' => 'deck',
        ],
    ])

    ->addFields(get_field_partial('partials.add_class'))
    ->addFields(get_field_partial('partials.wildcard_grid'))


    ->addFlexibleContent('pieces', ['button_label' => 'Add Pieces'])

        //Pre Header
        ->addLayout('preheader')
            ->addText('preheader')

        // Header
        ->addLayout('header')
            ->addText('header')

        // WYSIWYG
        ->addLayout('wysiwyg')
            ->addWysiwyg('paragraph', [
                'label' => 'Wysiwyg',
            ])
                
        // Image
        ->addLayout('image')
            ->addImage('image')

        //Button
        ->addLayout('button')
            ->addFields(get_field_partial('modules.button'))

        //Wrapper Open
        ->addLayout('wrapper open')
            ->addFields(get_field_partial('partials.add_class'))

        //Wrapper Close
        ->addLayout(get_field_partial('modules.wrapper_close'));

return $wildcard;
        