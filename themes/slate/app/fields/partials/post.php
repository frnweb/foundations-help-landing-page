<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('post');

$post
    ->setLocation('post_type', '==', 'post')

    ->addText('author_name', [
		'label' => 'Article Author'
	]);

return $post;