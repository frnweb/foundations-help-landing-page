jQuery(function($) {
    jQuery( document ).ready(function() {
        window.jQuery = $;
        window.$ = $;

        // Sets the map center over Kansas City
        var latlng = new google.maps.LatLng(41.850033, -87.6500523);
        var mapOptions = {
          zoom: 4,
          center: latlng
        }

        // Initializes new map and passes it mapOptions credentials
        var map;

        //Stores makersArray to be cleared on new search
        var markersArray = [];

        //Creates new marker window on click. 
        var infoWindow = new google.maps.InfoWindow;

        //Sets toggle to show and hide map/card on radio click

            //CARD TOGGLE
            $(".sl_toggle--card").click(function() {
                $(".sl_toggle--card").addClass('sl_active');
                $(".sl_toggle--map").removeClass('sl_active'); 

                //DECK
                $(".mapShow").hide();
                $(".sl_location__deck").show();
            });

            //MAP TOGGLE
            $(".sl_toggle--map").click(function() {
                $(".sl_toggle--map").addClass('sl_active');
                $(".sl_toggle--card").removeClass('sl_active');  

                //DECK
                $(".sl_location__deck").hide();
                $(".mapShow").show();
                map.setZoom(4);
            });

            // IE 10 or OLDER ERROR MESSAGE
            if(window.navigator.userAgent.indexOf('MSIE ') > 0) {
                $(".sl_facility__header").append('<div class="sl_ie-error"><p>You are using a browser that is not supported by the Google Maps JavaScript API. Consider changing your browser. <a href="https://browsehappy.com/">Upgrade your browser.</a></p></div>');
            }


        //Function iterates over myFacilityData objects and creates markers based on Lat/Lng. Also sets Marker card content, and adds event listener. setContent only accepts string, not object, can't append.

        //Check if myFacilityData is an object that exists on window load, grabs markers and markup for all facilities from myFacilityData object passed from twig template, loads map.
        if (window.myFacilityData) {
            map = new google.maps.Map(document.getElementById('map'), mapOptions);
            for(let i = 0; i < myFacilityData.length; i++){
                    getMarkers(myFacilityData[i]); 
            }
        }

        function getMarkers(myFacilityData){
            
            var contentString = '<div class="sl_card sl_card--map">' +
                '<div class="sl_card__image" style="background-image:url(' + myFacilityData.facilityImage + ')">' + 
                '</div>' + 
                '<div class="sl_card__content">' + 
                '<h5>' + myFacilityData.facilityCity + ', ' + myFacilityData.facilityState + '</h5>' +
                '<h3>' + myFacilityData.facilityName + '</h3>' +
                '<a href="' + myFacilityData.facilityUrl + '" class="sl_button sl_button--simple" target="_blank">Learn More</a>'
                '</div></div>'

            var icon = {
                url: '/wp-content/themes/slate/dist/img/icon/map-pin--secondary.svg',
                scaledSize: new google.maps.Size(30,45)
            }

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(myFacilityData.facilityLat, myFacilityData.facilityLng),
                animation: google.maps.Animation.DROP,
                icon: icon,
                map: map
            });

            marker.addListener('click', function() {
                infoWindow.setContent(contentString)
                infoWindow.setOptions({
                })
                infoWindow.open(map, marker);
            });

            markersArray.push(marker);
        }


    });
});