import 'babel-polyfill'
import whatInput from 'what-input'

import googleMaps from './lib/google-maps'
import locationModule from './lib/location-module'
import gaEvents from './lib/ga-events'
import slickCarousel from './lib/slick-carousel'
import hamburger from './lib/hamburger'
import contactMobile from './lib/contact-mobile'
import backTop from './lib/back-to-top'
import responsiveEmbed from './lib/responsive-embed'

jQuery(function($) {
    jQuery(document).ready(function() {
        $(document).foundation();
    });
});