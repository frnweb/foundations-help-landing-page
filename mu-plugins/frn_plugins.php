<?php
/**
 * Plugin Name: FRN Settings!
 * Description: This plugin installs the common website elements used on most of our sites. 
				 It includes phone number shortcodes, Google Analytics features, live chat options, context boxes, Chartbeat, and much more... 
				 This plugin can only be updated manually via FTP or ManageWP. If uninstalled, settings will remain in the DB.
 * Version: 2.12.2
 * Author: Daxon Edwards / Foundations Recovery Network
 * Author URI: https://daxon.me
 **/
 
require_once( 'frn-plugin/frn_plugin.php' );