<?php

require_once('part_phone_infinity_pools.php'); //Pools and patterns functions

//////
// MAIN INFINITY ACTIVATION FUNCTION:
// 
function frn_infinity_activate($phones="") {
	//we can use the global var in functions
	//or use this function each time
	//function doesn't process if global var already set (making it light if called many times)
	//general decision was to call function each time just in case we change philosophy in the future
	global $inf_main;  //array(activate type,class,data)
	$activate="no"; $class=""; $data=""; $class_text=""; $data_text="";
	
	if($phones=="") {
		if(function_exists('frn_phone_old_to_new')) $phones = frn_phone_old_to_new();
	}
	if(!isset($phones['tracking'])) $phones_tracking="";
		else $phones_tracking=$phones['tracking'];
	if(!isset($phones['infinity'])) $inf_options="";
			else $inf_options=$phones['infinity'];
	if(!isset($inf_options['activate'])) $inf_options_activate="";
		else $inf_options_activate=$inf_options['activate'];

	if($phones_tracking=="dt-ads" || $phones_tracking=="infinity") {
		if($inf_options!=="") {
			if($inf_options_activate!=="" && $inf_options_activate!=="none") {

				if($inf_options_activate=="js"){
					$activate = $inf_options_activate;
				}
				elseif($inf_options_activate=="all") {
					//if "all", these are the default values
					// "###"" replaced with discovery number
					$activate = $inf_options_activate;
					$class = "InfinityNumber"; 
					$data  = "data-ict-discovery-number=\"###\" data-ict-silent-replacements=\"false\" ";  
					$class_text = "InfinityNumber call-us visible-xs";
					$data_text  = "data-ict-discovery-number=\"###\" data-ict-silent-replacements=\"true\" ";
					
					//If DT should be triggered by st-t url vars...
					if($phones_tracking=="dt-ads") {

						//Deactivate Infinity pools if:
							//DT var (st-t) is present in URL or the cookie value is stored
							//Deactivating pools is as simple as changing the value to JS
						
						//Although the DT activate function tests for url tag, we need to do it here
						if(function_exists('frn_phone_get_url_tag')) {
							$tag=frn_phone_get_url_tag();
						}
						if(!isset($tag['tag'])) $tag_tag="";
							else $tag_tag=$tag['tag'];
						if(!isset($tag['value'])) $tag_value="";
							else $tag_value=$tag['value'];
						
						$cookie=frn_infinity_get_cookie();
						if(!isset($cookie['pool_type'])) $cookie_pool="";
							else $cookie_pool=$cookie['pool_type'];
						if(!isset($cookie['pool_value'])) $cookie_value="";
							else $cookie_value=$cookie['pool_value'];
						
						if(($cookie_pool=="st-t" || $tag_tag=="st-t") && $cookie_value!=="cancel" && $tag_value!=="cancel") { 
							//echo "Cookie pool type: ".$cookie['pool_type'];
							//echo "<br />DT Activated due to URL tag";
							$activate="js"; //changes "all" to "js" to deactivate phone number replacement, but keep Infinity's analytics alive AND DT JS
							$class="";
							$data="";
							$class_text="";
							$data_text="";
						}
					}
				}
			}
		}
	}

	//sets global variable
	$inf_main = array(
			'activate'		=> $activate,
			'class'			=> $class,
			'data'			=> $data,
			'class_text'    => $class_text,
			'data_text'     => $data_text
		);
	//print_r($inf_main);

	return $inf_main;
}

function frn_infinity_pool_num($sc="",$phones="") {
	//This function is called within the primary "frn_phone_number" function that gets the site's main number
	//This function's purpose is only to get an infinity auto-discovery number (i.e. it won't get the manually entered number from the db)
	$inf_number=""; $tracking="";
	if($phones=="") $phones = get_option('phones');
	if(isset($phones['tracking'])) $tracking = $phones['tracking'];

	//To provide a fallback in case the Infinity system is down, the system will continue providing Talkdesk numbers. (i.e. that's why we aren't using $inf_main)
	//The Analytics element will turn off the pools feature.
	if($tracking=="dt-ads" || $tracking=="infinity") {
		//get main Infinity pool (automated selection is only on admin, infinity not activated until saved)
		//if a value is anything but "all", it means pools are deactivated
		//sc used when function is called from the shortcode processor
		if($sc!=="") {
			if(stripos($sc, "_sms") && function_exists('frn_infinity_sms_by_sc')) {
				$inf_number = frn_infinity_sms_by_sc($sc,$phones);
			}
			else {
				$inf_number = frn_infinity_number_by_sc($sc,$phones); //get infinity number using the shortcode
			}
			if($inf_number=="") { //repeat in case SC number not provided - makes sure main pool pulled
				$inf_number = frn_infinity_main_pool($phones);
			}
		}
		else {
			//Infinity system is on.
			//but no shortcode is used
			//Instead of main number, this defaults to the main pool.
			$inf_number = frn_infinity_main_pool($phones);
		}
		
		//return number in our core format since numbers in finity are stored without formatting
		if($inf_number!=="") {
			if(function_exists('frn_phone_format')) {
				$phones_format="-";
				if(isset($phones['infinity']['format'])) $phones_format = $phones['infinity']['format'];
				$inf_number=frn_phone_format($inf_number,$phones_format); 
			}
		}
	}

	return $inf_number;
}
function frn_infinity_main_pool($phones="") {
	$inf_number=""; $pool_id="";
	
	//Uses global number to reduce multiple processing between all shortode uses
	global $frn_main_number;
	$frn_main_number=trim($frn_main_number); //sometimes it's blank because of a space
	if($frn_main_number!=="") {
		$inf_number = $frn_main_number; //keeps all the infinity elements from processing over and over again as the page loads
	}
	
	if($inf_number=="") {
		if($phones=="") $phones = get_option('phones');
		//main_pool only stores pool ID so we can change names as needed without having to change a lot
		if(isset($phones['infinity']['main_pool'])) $pool_id = $phones['infinity']['main_pool'];
		//echo "Pool ID: ".$pool_id;
		if($pool_id!=="") {
			$inf_number=frn_infinity_mitel_td($pool_id,$phones);
		}
	}

	//The first time a phone number is called, this var is blank. 
	//This makes sure the main number is stored there if blank
	//echo "main number should only show once: ".$frn_main_number;
	if($frn_main_number=="") {
		$frn_main_number = $inf_number;
	}

	return $inf_number;
}
function frn_infinity_url_to_sc($sc=""){
	$url_trigger=frn_infinity_get_cookie(); //get URL tag values --whether in url, cookie, or session
	//print_r($url_trigger);
		//keys in array
		//$url_trigger=array('pool_type' => '', 'pool_value' => '' );

	if(!isset($url_trigger['pool_value'])) $pool_value="";
		else $pool_value=$url_trigger['pool_value'];
	
	//bd check
	if($pool_value=="bd") {
		$sc="bd";
	}
	//local check
	else {
		$iop_check=stripos($sc,"iop_");
		if($pool_value=="local" && $iop_check===false) {
			$sc=$sc."_local";
		}
	}
	
	return $sc;
}

function frn_infinity_pool_by_kw($text_to_search="",$type="number",$phones="") {
	$pool=""; $i=0; 
	if($text_to_search!=="") {
		global $infinity_pools;				
		if(isset($infinity_pools)) {
			while($pool=="" && $i<count($infinity_pools)) {
				if(stripos($text_to_search,$infinity_pools[$i]['kw'])!==false){
					$pool = $infinity_pools[$i][$type];
					if($type=="number" && function_exists('frn_phone_format')) {
						$phones_format="-";
						if($phones=="") $phones = get_option('phones');
						if(isset($phones['infinity']['format'])) $phones_format=$phones['infinity']['format'];
						$pool = frn_phone_format($pool,$phones_format);
					}
				}
				$i++;
			}
		}
	}
	return $pool;
}

function frn_infinity_number_by_sc($sc="",$phones="") {
	$inf_number="";
	global $infinity_pools; 
	if(isset($infinity_pools)) {
		//echo "shortcode: " . $sc . "<br />";
		$sc_base= str_replace("_phone", "", $sc); //get down to core sc for searching
		//echo "shortcode base: " . $sc_base . "<br />";
		$sc_base = frn_infinity_url_to_sc($sc_base); //changes sc if URL tag for local or bd provided

		if($sc_base=="frn") {
			//The shortcode is frn_phone, that's used on all sites regardless of brand.
			//As a result, we can't expect it to always be the main FRN phone. 
			//Instead, we need to resort to the main pool selected in the FRN settings. 
			//That maintains the original purpose of that shortcode.
			$inf_number = frn_infinity_main_pool($phones);
		}
		else {
			$key = array_search(strtolower($sc_base), array_column($infinity_pools, 'sc')); //searches pools by sc to return key/id
			//print_r(array_column($infinity_pools, 'sc'));
			//echo "key: " . $key . "<br />";
			if($key!==false) {
				return frn_infinity_mitel_td($key,$phones,$infinity_pools);
			}
		}
	}
	return $inf_number;
}

function frn_infinity_sms_by_sc($sc="",$phones="") {
	$inf_number="";
	global $infinity_pools; 
	if(isset($infinity_pools)) {
		//echo "shortcode: " . $sc . "<br />";
		$sc_base= str_replace("_sms", "", $sc); //get down to core sc for searching
		//echo "shortcode base: " . $sc_base . "<br />";
		$key = array_search($sc_base, array_column($infinity_pools, 'sc')); //searches pools by sc to return key/id
		//print_r(array_column($infinity_pools, 'sc'));
		if($key!==false) {
			//echo $key;
			$inf_number = frn_infinity_sms($key,$infinity_pools);
			if($inf_number!=="") {
				if(function_exists('frn_phone_format')) {
					$phones_format="-";
					if($phones=="") $phones = get_option('phones');
					if(isset($phones['infinity']['format'])) $phones_format=$phones['infinity']['format'];
					$inf_number=frn_phone_format($inf_number,$phones_format);
				}
			}
		}
	}
	return $inf_number;
}
function frn_infinity_sms($pool_id="0",$infinity_pools="") {
	if($infinity_pools=="") global $infinity_pools;
	return $infinity_pools[$pool_id]['sms']; //grabs sc associated number from the array using key/id
}


function frn_infinity_pool_name_by_number($number="",$phones="") {
	$pool_name=""; $num_field="mitel"; //default for first install
	if($number!=="") {
		global $inf_main;
		if($inf_main['activate']!=="no") {
			$number = frn_phone_digits_only($number);
			if($phones=="") $phones = get_option('phones');
			$phones_discovery="";
			if(isset($phones['infinity']['discovery'])) $phones_discovery = $phones['infinity']['discovery'];
			if($phones_discovery=="td") { 
				$num_field="number";
			}
			global $infinity_pools; 
			if(isset($infinity_pools)) {
				//echo "number in ".$num_field. ": " . $number . "<br />";
				$key = array_search($number, array_column($infinity_pools, $num_field)); //searches pools by phone number to return key/id
				//print_r(array_column($infinity_pools, 'number'));
				if($key!==false) {
					return $infinity_pools[$key]['name'];
				}
			}
		}
	}
	return $pool_name;
}

function frn_infinity_discovery($number="",$phones="",$infinity_pools="",$num_field="mitel") {
	//Used to see if number provided is in the list of discovery numbers -- whether mitel or talkdesk depending on the settings.
	//If it's in the list, clean it up in order that Infinity can find it. (Infinity can't find numbers with a 1 or periods in formatting)
	if($infinity_pools==""){
		global $infinity_pools; 
	}
	if($phones=="") $phones = get_option('phones'); 
	if($infinity_pools!==""){
		$phones_discovery="";
		if(isset($phones['infinity']['discovery'])) $phones_discovery = $phones['infinity']['discovery'];
		if($phones_discovery=="td") $num_field="number";
		$key = array_search($number, array_column($infinity_pools, $num_field)); //searches pools by phone number to return key/id
		if($key!==false) {
			if(function_exists('frn_phone_format')) {
				$phones_format="-";
				if(isset($phones['infinity']['format'])) $phones_format = $phones['infinity']['format'];
				$number=frn_phone_format($number,$phones_format); 
			}
			else {
				//periods are really the most common issue
				$number=str_replace(".","-",$number);
			}
		}
	}
	return $number;
}

function frn_infinity_mitel_td($pool_id="0",$phones="",$infinity_pools="") {
	if($phones=="") $phones = get_option('phones');
	if($infinity_pools=="") global $infinity_pools;
	if(!isset($phones['infinity']['discovery'])) $phones['infinity']['discovery']="";
	if($phones['infinity']['discovery']=="mitel" || $phones['infinity']['discovery']=="") { 
		//empty since this will only happen when rolling it out the first time and we need the mitel numbers there instead.
		return $infinity_pools[$pool_id]['mitel'];
	}
	else {
		return $infinity_pools[$pool_id]['number']; //grabs sc associated number from the array using key/id
	}
}

function frn_infinity_number_replace($number="",$phones="",$sc="") {
	//Should ONLY work for numbers when the "number" attribute is provided
	
	//If DialogTech is activated (whether by url or manual selection, we need to keep old numbers active...
	//...since that system is set up to look for them and they'll be ported to Infinity)
	$dt_activated = frn_dialogtech_activate();
	if($dt_activated=="yes") return $number;
	//echo "DT deactivated<br />";
	
	//whether or not the Infinity system has activated all or js (i.e. pools deactivated) is choosen, 
	//we still need to change from the old number to the facility's new Talkdesk number so we can be sure someone will answer the phone call.


	if($number!=="") {
		$current_number = frn_phone_digits_only($number);
		//remove first 1 if number 1111111111
		if(strlen($current_number)>10 && frn_left_chars($current_number,1)=="1") {
			$current_number=frn_right_chars($current_number,strlen($current_number)-1);
		}
		if(function_exists('frn_phone_old_numbers')) {
			$old_numbers    = frn_phone_old_numbers();
		}
		if($phones=="") {
			if(function_exists('frn_phone_old_to_new')) $phones = frn_phone_old_to_new();
		}
		$i=0; $pool_number=""; //echo "current number: ".$current_number."<br />";
		while($pool_number=="" && $i<count($old_numbers)) {
			$old_number=$old_numbers[$i][0];
			//echo "number #".$i.": ".$old_number."<br />";
			if($old_number==$current_number) {
				//if no tracking service provided (i.e. tracking=none) then default to the main number
				if($phones['tracking']=="none") {
					if(!isset($phones['site_phone'])) $phones['site_phone']="";
					return $phones['site_phone'];
				}
				$old_code=$old_numbers[$i][1];
				$sc = frn_infinity_pool_by_kw($old_code,"sc",$phones); //returns pool shortcode if it's kw is found in old call-in clode
				//echo "found the shortcode (".$sc.") in old_code (".$old_code.")";
				$sc = frn_infinity_url_to_sc($sc); //checks if sc should change because of the url tag (changes to bd or adds _local to it)
				$pool_number = frn_infinity_number_by_sc($sc,$phones); //gets pool number for altered sc
				if($pool_number!=="") {
					//return number in our core format since numbers in finity are stored without formatting
					if(function_exists('frn_phone_format')) {
						$phones_format="-";
						if($phones=="") $phones = get_option('phones');
						if(isset($phones['infinity']['format'])) $phones_format=$phones['infinity']['format'];
						$pool_number=frn_phone_format($pool_number,$phones_format);
					}
				}
			}
			$i++;
		}
		if($pool_number!=="") return $pool_number;
	}
	return $number;
}
function frn_left_chars($str, $length) {
     return substr($str, 0, $length);
}
 
function frn_right_chars($str, $length) {
     return substr($str, -$length);
}



//////////////////////////////////
// Infinity SCRIPT/OPTIONS   //
//////////////////////////////////
if(!is_admin()) add_action('wp_head', 'frn_infinity_js', 101);
function frn_infinity_js(){
	if(!is_admin()) {
		$phones = get_option('phones');
		global $inf_main; //array(activate type,class,data)
		//echo "<br />Infinity activation: ".$inf_main['activate']."<br />";
		if($inf_main['activate']=="all" || $inf_main['activate']=="js") 
		{

			if(!isset($phones['infinity']['js']))  $js="";
				else $js=$phones['infinity']['js'];
			if(!isset($js['ga']))  $ga="";
				else $ga=$js['ga'];
			if(!isset($js['vwo'])) $vwo="";
				else $vwo=$js['vwo'];
			if(!isset($js['dc']))  $dc="";
				else $dc=$js['dc'];
			?>


	<!-- #####
	Infinity Tracking Options v2.0
	##### -->
	<?php if($dc!=="") { ?>
	
	<!--Double Click Integration-->
	<script type="text/javascript">
	var ictDcmIntegration = function() {
	    var dcmCookie = _ictt.push(['_retrieveItem', 'dcm_gid']);
	    if (dcmCookie) {
	        return dcmCookie;
	    }
	    if (!_ictt.push(['_get', 'infinityDcmScriptLoaded'])) {
	        _ictt.push(['_dropScriptTag', 'cm.g.doubleclick.net/pixel?google_nid=infinity_dmp&google_cm']);
	        _ictt.push(['_set', 'infinityDcmScriptLoaded', true]);
	    }
	}
	</script>
	<!--END Double Click Integration-->
	<?php } ?>
	<?php if($vwo!=="") { ?>
	
	<!-- VWO Infinity Callback -->
	<script type="text/javascript">
	var _ictVwoIntegration = function(key) {
	    if (key !== undefined && _ictt !== undefined && _vis_opt_readCookie != undefined) {
	        var vwoCombination = _vis_opt_readCookie('_vis_opt_exp_'+window[key]+'_combi');

	        if (typeof _vwo_exp[window[key]] != 'undefined' && typeof(_vwo_exp[window[key]].combination_chosen) != 'undefined') {
	            vwoCombination = _vwo_exp[window[key]].combination_chosen;
	        }
	        if (vwoCombination !== '' && vwoCombination !== null) {
	            _ictt.push([ '_setCustomVar', ['vwocomb', vwoCombination] ]);
	        }
	        _ictt.push([ '_setCustomVar', ['vwoexp', window[key]] ]);
	    };
	}
	</script>
	<!-- END VWO Infinity Callback -->
	<?php } ?>
	
	<!-- Copyright Infinity 2018 www.infinitycloud.com -->
	<script type="text/javascript">
	    var _ictt = _ictt || [];
	    _ictt.push(['_setIgrp','2728']); // Installation ID
<?php if($ga!=="") { ?>
		_ictt.push(['_enableGAIntegration',{'gua':true,'ga':false}]);
<?php } ?>
	    _ictt.push(['_includeExternal',[{'from':'ictDcmIntegration','to':'_setCustomVar','as':'dcm_gid'}]]);
	    _ictt.push(['_includeExternal',[{'from':'_vis_opt_experiment_id','to':'_ictVwoIntegration'}]]);
<?php if($inf_main['activate']=="all") { 
		//check if slideout tab activated (delay this push until that's done)
		?>
		_ictt.push(['_enableAutoDiscovery']);
		<?php
			global $frn_lhn_disable_tab;
			if($frn_lhn_disable_tab!=="Yes") {
			?>function LHN_HelpPanel_onLoad(){
			_ictt.push(['_enableAutoDiscovery']);
		}
		<?php }
} ?>_ictt.push(['_track']);
	(function() {
	    var ict = document.createElement('script'); ict.type = 'text/javascript'; ict.async = true;
	    ict.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'ict.infinity-tracking.net/js/nas.v1.min.js';
	    var scr = document.getElementsByTagName('script')[0]; scr.parentNode.insertBefore(ict, scr);
	})();
	</script>

	<!-- #####
	END Infinity Tracking Options
	##### -->

		
		
	<?php
		}
	}
}




//////
// ADD FACILITY-SPECIFIC SHORTCODES:
// Make sure infinity pools array is set up before the shortcodes are created
// This will happen whether or not infinity is activated (makes sure facility SCs in content don't show)
add_action( 'init', 'frn_infinity_sc_install' );
function frn_infinity_sc_install() {
	if(function_exists('frn_infinity_pools_list')) {
		global $infinity_pools;
		if(!isset($infinity_pools)) $infinity_pools=frn_infinity_pools_list();
		foreach($infinity_pools as $pool){
			if($pool['sc']!="") add_shortcode( $pool['sc']."_phone", 'frn_phone' );
			if($pool['sc']!="" && $pool['sms']!="") add_shortcode( $pool['sc']."_sms", 'frn_sms' );
		}
	}
}


//function not used. Instead, this is triggered in the scripts.js automatically when the conversion trigger is defined as chat or email
function frn_infinity_click_events($category="",$action="") {
	$ia="";	global $inf_main;
	if(!isset($inf_main['activate'])) $inf_main['activate']="";
	if($inf_main['activate']=="all" || $inf_main['activate']=="js") {
		$ia=" _ictt.push(['_customTrigger', '".$category."', {'t':'".$action."'}]);";
	}
	if($inf_main['activate']=="all" || $inf_main['activate']=="js") return "yes";
}


//////
// Infinity URL Parameter Functions
//////

//frn_infinity_url_cookie sets the cooking for frontend users
//without this, only the landing page would use a local number. This makes sure, at least for one day, that they'll continue to get the local version of the pools.
//unfortunately at the PHP level, we can't determine if the browser has cookies activated
//so, we have to set the cookie and PHP session at the same time
//but when checking for the cookie, if nothing returned, we can check on the php session
if(!is_admin()) add_action( 'init', 'frn_infinity_set_cookie' );
function frn_infinity_set_cookie() {
	$inf_main = frn_infinity_activate(); $url_trigger="";
	//echo "Infinity activate in set_cookie: ".$inf_main['activate'];
	//echo "<br /><b>PHP session in set_cookie:</b> ";
	//print_r($_SESSION);
	if($inf_main['activate']=="all" || $inf_main['activate']=="js") {
		//unset( $_COOKIE['frn_pool_php'] );
		if(function_exists('frn_phone_get_url_tag')) {
			$url_trigger = frn_phone_get_url_tag(); //finds the first infinity or DT tag in array that's present in the url (returns array(tag,value))
		}
		if(!isset($url_trigger['value'])) $url_value="";
			else $url_value=$url_trigger['value'];
		if($url_value!=="") { //i.e. if tag is found
			//echo "<br /><b>URL_Trigger in set_cookie:</b> ".$url_trigger['value'];
			$cookie_key = 'STYXKEY_frn_pool';
			//STYXKEY is required by pantheon. Without it, the cookie is not available to PHP.
			//This also will cause the version of the page using this cookie name to be seperately cached from normal urls.
			//https://pantheon.io/docs/caching-advanced-topics/
			$cookie_value = $url_trigger['tag']."##".$url_value;
			$cookie_exp = time() + DAY_IN_SECONDS;
			if($url_value=="cancel") {
				//Removes cookie for testing purposes so that this doesn't continue for the remainder of the day
				//unset( $_COOKIE[$cookie_key] );
				setcookie($cookie_key, "", time() - 3600, COOKIEPATH, COOKIE_DOMAIN, is_ssl() );
				if(isset($_SESSION['pool_type'])) {
					unset($_SESSION['pool_type']);
					unset($_SESSION['pool_value']);
					session_destroy();
				}
			}
			else {

				//echo "<br />Dax: SET cookie exp: ".date("m/d/Y h:i:s a", $cookie_exp) . " " . date_default_timezone_get();
				if(isset($_COOKIE[ $cookie_key ])) {
					//check value
					$cookie=explode("##",$_COOKIE[ $cookie_key ]);
					if(!isset($cookie[1])) $cookie[1]="";
					//echo $url_trigger['value']."; ".$cookie[1];
					if($url_value!==$cookie[1]) {
						//echo "<br />Cookie set due to value change: ".$cookie_key."; ".$cookie_value;
						setcookie( $cookie_key, $cookie_value, $cookie_exp, COOKIEPATH, COOKIE_DOMAIN, is_ssl() ); 
					}
				}
				else {
					//echo "<br />Dax: SET cookie: ".$cookie_key."; ".$cookie_value;
					//VERY IMPORTANT:
						//Pantheon will not cache any page in which a cookie is set.
						//to utilize caching capability, only set a cookie if it's expired. 
						//extending a cookie will only disable caching
						//https://pantheon.io/docs/cookies/
						//https://pantheon.io/docs/caching-advanced-topics/
					setcookie( $cookie_key, $cookie_value, $cookie_exp, COOKIEPATH, COOKIE_DOMAIN, is_ssl() ); 
				}
				
				if(!isset($_SESSION)) session_start(); //works even if session already started. lasts until 30 minutes of inactivity.
				//echo "<br />Dax: session check: ";
				//print_r($_SESSION);
				if(!isset($_SESSION['pool_type'])) $_SESSION['pool_type']="";
				if($_SESSION['pool_type']=="" || ($_SESSION['pool_value']!==$url_value)) { //isset required since this function would happen in any case if URL parameter is applied
					//echo "<br />Dax: SET session: ".$url_trigger['tag'];
					$_SESSION['pool_type']  = $url_trigger['tag'];
					$_SESSION['pool_value'] = $url_value;
				}
				
				//print_r($_SESSION);
			}
		}
		//echo "<b>NEW Cookie values stored in _COOKIE:</b> ";
		//print_r($_COOKIE);
	}
}

function frn_infinity_get_cookie() {
	//This is not working.
	//for some reason the cookie is set with PHP (or JS), but can't be read in $_cookie. Nothing on the web reveals why. Very rare issue. 1/23/19
	//if current page has a URL trigger, the cookie may not be set by this point.

	$url_trigger=""; $cookie_key='STYXKEY_frn_pool';
	//STYXKEY is required by pantheon. Without it, the cookie is not available to PHP.
	//This also will cause the version of the page using this cookie name to be seperately cached from normal urls.
	////https://pantheon.io/docs/caching-advanced-topics/
	if(!isset($_COOKIE[ $cookie_key ])) $_COOKIE[ $cookie_key ]="";
	//echo "<br /><b>Cookie values returned in get_cookie function:</b> ";
	//print_r($_COOKIE);
	//echo "<br /><b>Dax: Get cookie:</b> ".$_COOKIE[$cookie_key];
	//echo "Does _COOKIE[$cookie_key] return anything? It would show here: \"".$_COOKIE[ $cookie_key ]."\"<br />";

	// (1) Check for URL tag -- no need to check for cookie if present
	if(function_exists('frn_phone_get_url_tag')) {
		$url_trigger = frn_phone_get_url_tag(); //finds the first infinity or DT tag in array that's present in the url (returns array(tag,value))
	}
	if(!isset($url_trigger['value'])) $url_value="";
		else $url_value=$url_trigger['value'];
	if($url_value!=="") { //i.e. if tag is found
		$url_trigger=array(
			'pool_type' => $url_trigger['tag'],
			'pool_value' => $url_value
		);
		return $url_trigger;
	}

	// (2) if url tag not present, check for cookie
	if($_COOKIE[ $cookie_key ]!=="") {
		$url_trigger=explode("##",$_COOKIE[ $cookie_key ]);
		$url_trigger=array(
			'pool_type' => $url_trigger[0],
			'pool_value' => $url_trigger[1]
		);
		//echo "<br /><b>Get cookie array:</b> ";
		//print_r($url_trigger);
		if($url_trigger['pool_value']=="cancel") {
			return "";
		} else {
			return $url_trigger;
		}
	}

	// (3) if cookie not active use PHP session as backup
	else {
		//print_r($_SESSION);
		if(!isset($_SESSION['pool_type'])) {
			$_SESSION['pool_type']="";
			$_SESSION['pool_value']="";
		}
		if($_SESSION['pool_value']!=="") {
			if($_SESSION['pool_value']=="cancel") {
				unset($_SESSION['pool_type']);
				unset($_SESSION['pool_value']);
				return "";
			} else {
				return array(
					'pool_type' => $_SESSION['pool_type'],
					'pool_value' => $_SESSION['pool_value']
				);
			}
		}
		
	}

	return "";
}

?>