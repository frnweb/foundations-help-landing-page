<?php


/*

v 1.1 - 10/23/17
v 1.2 - 2/1/18 - moved intro text below report, improved intro text into two paragraphs and clarified Relevanssi limitation (spelling corrections too)
v 1.3 - 6/12/18 - Improved twitter shortcode search to include the bracket due to too many false positives using twitter urls.

limits:
Will not search custom fields
Will not search plugins or theme settings
Only searches published posts
Limited to PHP files only
*/


function frn_shortcodes_admin() {

	if(is_admin()) { //just making sure this more intensive action doesn't happen for regular page loads.

		///////////
		/// FRN PLUGIN SHORTCODES ONLY
		//////////
		$facility_shortcodes=array();
		if(function_exists('frn_load_pools')) {
			global $infinity_pools; 
		 	foreach($infinity_pools as $pool) {
		 		if($pool['sc']!=="") $facility_shortcodes[]="[".$pool['sc']."_phone";
		 		if($pool['sms']!=="") $facility_shortcodes[]="[".$pool['sc']."_sms";
		 	}
		 }
		//what to search content for
		 //"[frn_phone",  //sometimes frn_phone is in comments or fields like %%frn_phone%%, so we need the bracket
		$plugin_shortcodes=array(
			"lhn_inpage",
			"frn_social",
			"/frn_social",
			"[twitter",
			"[/twitter",
			"frn_footer",
			"frn_privacy_url",
			"frn_boxes",
			"idomain",
			"ldomain",
			"frn_sitebase",
			"frn_imagebase",
			"frn_related",
			"frn_related_list",
			"frn_lhn"
		);
		$all_plugin_shortcodes=array_merge($facility_shortcodes,$plugin_shortcodes);
		//print_r($all_plugin_shortcodes);
		?>
		<ul class="frn_level_1">
			<?php 
			$toc = get_option('site_toc');
			if(!isset($toc['activation'])) $toc['activation']="";
			if($toc['activation']=="yes" || $toc['activation']=="sc") {
			?>
				<li><b><a href="/wp-admin/admin.php?page=frn_toc">Table of Contents (TOC)</a></b> are automatically activated in the site. Be sure to check those pages. </li>
			<?php }
			$rp = get_option('site_rp');
			if(isset($rp)) {
			if(!isset($rp['activation'])) $rp['activation']="";
			if($rp['activation']=="yes") { ?>
				<li><b><a href="/wp-admin/admin.php?page=frn_related_section">Related posts</a></b> are automatically added to many pages. Be sure to check them. </li>
			<?php }} ?>
		</ul>

		<?php


		///////////
		/// ALL REGISTERED SHORTCODES
		//////////

		//POSTS
		?>
		<div class="frn_rounded_box" style="float: left; margin-right: 15px;">
			<h4 style="margin:0;">POSTS: Phone Shortcodes Found</h4>
			<?php 
			
			frn_admin_sc_search_posts($all_plugin_shortcodes,"plugin_shortcodes");

			?>
		</div>
		<?php


		//PHP: FRN Plugin shortcodes only
		$found_in_files = frn_admin_sc_search_php($all_plugin_shortcodes);
		$found_count = count($found_in_files);
		$theme = wp_get_theme();
		?>
		<div class="frn_rounded_box" style="float: left; margin-right: 15px;">
			
			<?php if($found_count>0) { ?>
			
			<h4 style="margin:0;">PHP FILES: Phone Shortcodes Found</h4>
			
			<?php frn_admin_sc_search_php_display($found_in_files); ?>
			
			<?php } else { ?>

			<h4 style="margin:0;">No FRN Plugin shortcodes found in PHP files.</h4>
			
			<?php } ?>

		</div>
		<div style="clear:both;"></div>

		<?php



		///////////
		/// ALL REGISTERED SHORTCODES
		//////////
		// Everything registered on the site
		// Except for shortcodes registered in the plugin
		global $shortcode_tags;
	    $all_shortcodes = array_keys($shortcode_tags);
	    sort($all_shortcodes);
	    $shortcodes_cleaned=array();
	    foreach($all_shortcodes as $shortcode){
	    	if(!array_search($shortcode,$all_plugin_shortcodes)) {
	    		$shortcodes_cleaned[]=$shortcode;
	    	}
	    }

		//POSTS
		?>
		<div class="frn_rounded_box" style="float: left; margin-right: 15px;">
			<h4 style="margin:0;">POSTS: All Shortcodes Found</h4>
			<?php 
			
			frn_admin_sc_search_posts($shortcodes_cleaned,"all_shortcodes");

			?>
		</div>
		<?php
		//PHP
		$found_in_files = frn_admin_sc_search_php($shortcodes_cleaned);
		//$found_count = count($found_in_files);
		//$theme = wp_get_theme();
		?>
		<div class="frn_rounded_box" style="float: left; margin-right: 15px;">
			
			<?php if($found_count>0) { ?>
			
			<h4 style="margin:0;">PHP FILES: All Shortcodes Found</h4>
			
			<?php frn_admin_sc_search_php_display($found_in_files); ?>
			
			<?php } else { ?>

			<h4 style="margin:0;">No shortcodes found in PHP files.</h4>
			
			<?php } ?>

		</div>

		<div style="clear:both;"></div>

		<p>The report above tells the pages or files this plugin's shortcodes are used. 
		If you disable this plugin, you'll know what elements are be affected. 
		When scanning files, it only scans PHP files in the active theme's directory.
		The number of directory levels doesn't matter. It scans them all.
		</p>
		<p>
		For the pages/posts search, this system uses WordPress's default search engine--not Relevanssi. WordPress looks for the exact characters, while Relevanssi removes the underscore when searching. If you see hundreds of results, it's likely that the option to use Relevanssi in the admin is turned on in Relevanssi settings.
		At this time, this report will not scan for shortcodes used in plugin files/settings or custom fields, such as those created with ACF. (<a href="https://adambalee.com/search-wordpress-by-custom-fields-without-a-plugin/" target="_blank">future resource to help with including custom fields</a>)
		</p>

		<?php
	}

}

//Not Used
	//Tried to get query to search more post types, but this option didn't work (not sure why)
	function frn_admin_sc_search_post_types( $query ) {
	    //Modifies the default WP search to search more than just posts.
	    if ( $query->is_search ) {
	        $query->set( 'post_type', array( 'page', 'post' ) );
	    }
	    return $query;
	}
	//add_filter( 'pre_get_posts', 'frn_sc_search_post_types' );
//

function frn_admin_sc_search_posts($found_array,$source="") {
	//print_r($found_array);
	///Search all content first
	$args = array('public' => true );
	$types = get_post_types( $args, 'names', 'AND' );
	//echo "<br /><br />Post Types: ";
	//print_r($types);
	$others=""; $found=""; $dnis_name="";
	$source_text=str_replace("_"," ",$source);
	if(stripos($source,"number")!==false) $oldnumbers=frn_phone_old_numbers();
	?>
		<ol class="frn_level_1">
			<?php 
			$total_found = 0;
			$others = "";
			$item_prev="";
			foreach($found_array as $item) {
				if(trim($item)!=="") {
					$total = 0;
					//$type = "page";
					//$item = 'frn_phone';
					if(stripos($source,"shortcode")!==false) $search_item_show="[".str_replace("[","",$item)."]";
					elseif(stripos($source,"number")!==false) {
						$search_item_show=frn_phone_patterns($item);
						if($oldnumbers!=="") {
							$key = array_search($item, array_column($oldnumbers, 0)); //get old Mitel DNIS name
							//print_r(array_column($infinity_pools, 'number'));
							if($key!==false) {
								$dnis_name = " - ".$oldnumbers[$key][1];
							}
						}
						$item=substr($item,3,3)."-".substr($item,6,4);
					}
					else $search_item_show=$item;
					//echo " [before: ".$item_prev."]";
					foreach($types as $type) {
						if(trim($type)!=="") {
							if($type!=="attachment" || $type!=="events") {
								
								$sc_results = new WP_Query( array( 
									's' 				=> $item,  
									'post_type' 		=> $type,
									'posts_per_page' 	=> -1,
									'post_status' 		=> array( 'publish', 'pending', 'draft', 'future' ),
									'fields'			=> 'ids' //keeps the search light - no HTML, no filters, no SC processing in content, etc.
									//'orderby'			=> 'post_type',
									//'order'				=> 'ASC'
								));

								$total=$sc_results->found_posts;
								if ( $sc_results->have_posts() ) {
									//the list begins so deep in this function because we don't want an sc to show unless it's found within a type.
									//at this stage, the search is looping through each post type registered with WP
									if($item!==$item_prev) { 
										if($total_found!==0) echo "</li>"; //close list item if it's after the first loop
										//sets prev SC on the first post type loop so the next time around the SC is not printed to the HTML
										$item_prev=$item; 
										?>
										<li><b><?=$search_item_show;?></b><?=$dnis_name;?>: 
										<?php 
									} 
									$cnt=0; 
									while ( $sc_results->have_posts() ) {
										$sc_results->the_post(); $cnt++; $total_found++;
										if($cnt==$total) { ?>
				<a href="/wp-admin/edit.php?s=<?=$item;?>&post_status=all&post_type=<?=$type;?>" target="posts_search" ><?=$cnt;?> <?=$type;?><?=($cnt>1) ? "s" : "" ;?></a>; 
										<?php } 
									} 
									/* Restore original Post Data */
									//wp_reset_postdata(); -- not needed since this isn't a post
								}
							}
						}
					}
					$comma=",";
					if($item!==$item_prev && $total==0 && trim($search_item_show)!=="") {
						if($others=="") $comma="";
						$others.=$comma.$search_item_show;
					}
					//item_prev is not a duplicate from the version above. It's necessary to establish for every SC search after the first one.
					//$item_prev=$item; 
					//echo " [after: ".$item_prev."]";
				}
			}
			?>
			</li>
		</ol>
		<?php 
		if($total_found==0) { ?>
			<br /><b>[No <?=$source_text;?>s found in any post (but may still be in custom fields)]</b>
			<?php } else { ?>
			Total found: <?=$total_found;?>
		<?php } ?>

		<?php if($others!=="") {
			//print_r($others);
			$others = explode(",",$others);
			$others_cnt=count($others);
			?>

	<div style="clear:both;margin-top:15px;">[<a href='javascript:showhide("<?=$source;?>_notfound");'><?=$others_cnt;?> <?=$source_text;?> not found</a>]</div>
	<br />
	<div id="<?=$source;?>_notfound" style="display:none; clear:none;" >
		<ol class="frn_level_1" >
		<?php foreach($others as $other) { 
			if(trim($other)!=="") { 
				if(stripos($source,"shortcode")!==false) $other="[".$other."]";
				elseif(stripos($source,"number")!==false) {
					if($oldnumbers!=="") {
						$other=frn_phone_digits_only($other);
						$key = array_search($other, array_column($oldnumbers, 0)); //get old Mitel DNIS name
						//print_r(array_column($infinity_pools, 'number'));
						if($key!==false) {
							$dnis_name = " - ".$oldnumbers[$key][1];
						}
					}
					$other=frn_phone_patterns($other);
				}
				?>
			<li><b><?=$other;?></b><?=$dnis_name;?></li>
			<?php }
		} ?>
		</ol>
	</div>
	<?php
	}
	 
}


//RELEVANSSI MODIFICATION
	//makes sure literal shortcodes are searched instead of just letters
	if(function_exists('relevanssi_do_query')) add_filter('relevanssi_remove_punctuation', 'frn_admin_relevanssi_punct_conv', 9);
	function frn_admin_relevanssi_punct_conv($a="") {
		$a = str_replace('_', 'UNDERSCORE', $a);
	    return $a;
	}
	if(function_exists('relevanssi_do_query')) add_filter('relevanssi_remove_punctuation', 'frn_admin_relevanssi_punct_revert', 11);
	function frn_admin_relevanssi_punct_revert($a) {
	    $a = str_replace('UNDERSCORE', '_', $a);
	    return $a;
	}
//END RELEVANSSI


function frn_admin_sc_search_php($shortcodes=array(), $dir="", $count=0 , $return="yes") {
	//also used for Infinity old numbers searching in admin_submenu_phone.php
	//echo "Dax PHP Shortcodes: <br />";
	//print_r($shortcodes);
	if($return!=="yes") {
		global $found_php;
		global $found_count;
	}
	if(!isset($found_php)) $found_php=array();
	if(!isset($count)) $count=0;
	if(!isset($found_count)) $found_count=0;

	//use theme directory if none provided
	if($dir=="") $dir=get_template_directory();
	//$plugins_dir = get_home_path()."wp-content/plugins"; - didn't work when quickly tested 1/24/19
	//echo $dir."<br />";

    //setting a 1 minute timeout in case there is an issue
    ini_set('max_execution_time', 30); //in seconds

    //echo "<script>var elm = document.getElementById('search');elm.value='$_POST[search]';</script>";

    //get array of files in the directory
    $files = scandir($dir);
    //print_r($files);
    foreach ($files as $file) {
    	$path = realpath($dir . DIRECTORY_SEPARATOR . $file);
        //make sure current file or directory isn't ones we know we don't want scanned
        if(
        	$file != "." && $file != ".." 
        	&& !strpos($path,"/uploads") 
        	&& !strpos($path,"/cache") 
        	//&& !strpos($path,"/plugins") 
        	&& !strpos($path,"/css") 
        	&& !strpos($path,"/images")
        	&& !strpos($dir,"/js")
        	&& $file!=="cache"
        	&& $file!=="css"
        ) {

        	$count++;
	        
	        //if the current path is not a directory, cycle through the files in the directory
	        if (!is_dir($path)) {
	        	
				//$filetype = wp_check_filetype($file);
				//print_r($filetype)."<br />";
				//echo "; ext: ".$filetype['ext']."<br />"; 
				//search content only if filetype is PHP
				if( strpos($file,".php")==true ) {

					//echo "<br />Scanning: ".$file;
		            $content = file_get_contents($path);
		            
		            //search content for shortcodes
		            $count_prior = $found_count;
		            $shortcode="";
		            foreach($shortcodes as $shortcode) {
		            	if(is_array($shortcode)) {
			            	if(is_numeric($shortcode[0])) {
			            		$shortcode=substr($shortcode[0],3,3)."-".substr($shortcode[0],6,4);
			            	}
			            }
			            if(trim($shortcode)!=="") {
				            if (strpos($content, $shortcode) !== false) {
				            	//if found, store in variable to print at end
				                array_push($found_php, array($shortcode,$file,$path) );
				                //$found_php[]=array($shortcode,$file,$path);  //no longer allowed in PHP v7+
				                //found_count is for the number of files affected, not number of shortcodes found
				                if($count_prior==$found_count) $found_count++;
				            }
				        }
			            //$results[] = $path;
			        }
	        	}

	        } elseif (
	        	$file != "." 
	        	&& $file != ".." 
	        	&& $file!=="uploads"
	        	&& $file!=="plugins"
	        	&& $file!=="css"
	        	&& $file!=="images"
	        	&& $file!=="js"
        	) {
	            //if directory, then run this function on files within the directory
	            //echo "<h4><b>/".$file."/ is a dir. Rerunning on its files.</b></h4>";
	            frn_admin_sc_search_php($shortcodes, $path, $count);
	            //$results[] = $path;
	        }
	    }
	    else {
	    	//echo $file." (skipped)<br />";
	    }
    }
    if($return=="yes") {
    	return $found_php;
    }

    //return $results;
}

function frn_admin_sc_search_php_display($list="",$search_type="") {
	//as much as we would like to control the display of both searches with one function,
	//we display the number of webpages/posts with shortcodes because the list gets really long with so many posts using shortcodes
	//we list PHP file names for the php search because there are fewer and it's more complicated to open files while posts are easy.
	$href=""; $theme="";
	//if($search_type=="php") {
		$dir   = get_template_directory(); 
		$theme = wp_get_theme();
		$theme_dir=get_template();
		//$theme_name = $theme->get( 'Name' );
		//$http = "http".((is_ssl()) ? "s" : "")."://";
	//}
	//elseif($search_type=="posts") {
	//	$href  = "/wp-admin/edit.php?s=%%term%%&post_status=publish&post_type=%%post_type%%";
	//}
	?>
		
		<ul class="frn_level_1">
			<?php 
			$found_cnt = count($list);
			$files_count=0; $term_count=0;
			$prior_path="";
			//echo "Shortcodes Found: ".$found_cnt;
			if($found_cnt>0) {
				//print_r($list);
				//echo "<br />";
				//echo "Count: ".count($list)."<br />";
				$found_prior=""; $i=1; $term_display="";
				foreach( $list as $item ) { 
					//if($search_type=="php") {
						//array($term,$file name,$path)
						$term = $item[0];
						$found = $item[1];
						$path = $item[2];
						if($prior_path=="") $prior_path=urlencode(str_replace($dir."/","",$path));
						$href  = "/wp-admin/theme-editor.php?file=%%file%%&theme=".$theme_dir;
						$href  = str_replace("%%file%%",$prior_path,$href);
						//echo "Prior: ".$found_prior."; Current: ".$found."<br />";
						//echo "<br /> Shortcode #".$i.": ".$term;
					//}
					//elseif($search_type=="posts") {
					//	$term = $item[0];
					//	$post_type = $item[1];
					//	$href=str_replace("%%post_type%%",$post_type,$href);
					//	$href=str_replace("%%term%%",$term,$href);
					//}
					
					if($found_prior==$found) {
						//same file
						$term_display.=", [".str_replace("[","",$term)."]";
						$term_count++;
					}
					if($found_prior!=$found || $i==$found_cnt) {
						//new set started, print the old set before starting the new list
						if($i>1) {
							$files_count++;
						?>
			<li><b><a href="<?=$href;?>" target="<?=$search_type;?>_window" ><?=$found_prior;?></a>:</b> <?=$term_display;?></li>
						<?php
						}
						$term_display="[".str_replace("[","",$term)."]";
					}
					$found_prior=$found; $i++;
					$prior_path=urlencode(str_replace($dir."/","",$path));
				}
			} ?>
		</ul>
		<div><?=$term_count;?> shortcodes found in <?=$files_count;?> <?=$theme;?> Theme Files</div>
		<?php
}