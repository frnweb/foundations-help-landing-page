<?php

/*
//Changes:
	v1.0 - initial version included in the main settings page
	v2.0 - 2/1/18:
		The report and notes were moved to a submenu
		Improved wording, directions, and spacing.
	
*/

// This PHP file adds the developer options SUB menu page to the main FRN Settings Menu
add_action('admin_menu', 'frn_plugin_subpage_dev');
function frn_plugin_subpage_dev() {
	
	add_submenu_page( 'frn_features',"Shortcodes Report and Notes for Developers", "Shortcodes Report", 'manage_options', 'frn_dev', 'frn_shortcode_report_section_text');
	add_settings_section('site_shortcode_report', '<a name="report"></a><br />Shortcodes Report and Notes for Developers', 'frn_shortcode_report_section_text', 'frn_shortcode_report_section');  //initially just shortcodes report -- changed it in admin, but to speed up dev left all functions shortcode related
		add_settings_field('frn_shortcode_report_button', "Activation <span style='white-space:nowrap;'><a href='javascript:showhide(\"frn_shortcode_report_help\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_shortcode_report_field', 'frn_shortcode_report_section', 'site_shortcode_report');
}




//////////
// DEVELOPER_Resources
//////////
	//no settings saved for this

if(is_admin()) include("admin_shortcodes_report.php");


function frn_shortcode_report_section_text() { ?>
	<div class="wrap">
		<h1>Shortcodes Reports and Notes for Developers</h1>

		<div class="intro_text">
			
			<p>Use the shortcodes report to find pages, posts, and PHP files that use shortcodes from this plugin.
			Custom fields are not searched, including those managed by ACF. Using Relevanssi may help find them, but the system removes underscores from shortcode names. The results may be quite inflated as a result.</p>
			<p>The list of functions below the report help developers know what elements they can use directly without having to recode something with similar features. This list has not been updated to the new phone shortcodes and infinity system.</p>

			<form class="frn_styles" id="frn_developer_notes">
				<h2>Shortcodes Reports</h2>
				<?php 
				// SHORTCODES_REPORT - It runs automatically when this page is loaded.
				frn_shortcodes_admin(); 
				?>
				
				<h2>Additional Functions You Can Use</h2>
				<?php 
				// ADDITIONAL_FUNCTIONS
				frn_developer_notes(); 
				?>

			</form>
		</div>
	</div>
	<?php
}


function frn_developer_notes() {
	//Referred to in the Shortcodes Report function. Figured there is no need to create a completely new section for something so simple.
	//Updated: 6/27/17
	?>
	
	<div class="intro_text">
		<ul class="frn_level_1">
			<li>Shortcodes are best to use even in PHP files since they provide you with one place to change them and there is a naturally nice failure. </li>
			<li>But if the help information provided via <img src="<?=$GLOBALS['help_image'];?>" /> or the Help Menu at the top mentions a function (e.g. Related Posts), use as you prefer.</li>
			<li><b>global $frn_mobile: </b>
				<ul class="frn_level_2">
					<li>Boolean: In an if statement, just use "global $frn_mobile; if($frn_mobile){}" to test if mobile or not. You may want to try an isset() check in case the FRN plugin is deactivated so that fewer error logs are created.</li>
					<li>If it's a desktop device, this will return false. Otherwise, the variable is filled out with "Smartphone" or "Tablet" in case you want to create a feature for either of them specifically. </li>
					<li>This variable was initially created for Live Chat. The variables are reported to the LiveHelpNow system.</li>
					<li>This variable is defined in frn_plugin.php using variables set in part_mdetect.php. You can use them directly if you preferred. I just wanted something easier to remember.</li>
					<li>The devices in part_mdetect.php hasn't been updated since well before June 2016. It needs an update, but the GitHub version is no longer supported. I have looked into <a href="https://github.com/serbanghita/Mobile-Detect" target="_blank">Mobile Detect Library</a>, but haven't added it to the plugin yet. It has more recent updates.</li>
				</ul>
			</li>
			<li><b>global $frn_company_ips: </b>
				<ul class="frn_level_2">
					<li>This is a global variable (global $frn_company_ips) and a function (frn_company_ips();) used to check if the current user is on a company network. </li>
					<li>It returns the IP address of the user if it's in our list of company IPs. Otherwise, it's blank. It's never false.</li>
					<li>The best approach is to use the global variable. It's less of a resource hog, but both are mentioned here your information. </li>
					<li>If you want to deactivate a feature (or add one), use this feature to do so. It's not perfect. </li>
					<li>Cali and Dax try to keep up with UHS IPs. This function uses a regular expression to capture most of the 90,000 UHS employees and we add remote Marketing employees as well. We only update this once a year or when we know there was a network change for UHS in PA.</li>
					<li>The function is located in part_analytics_features.php.</li>
				</ul>
			</li>
			<li><b>.js#async</b>
				<ul class="frn_level_2">
					<li>Add "#async" at the end of any JS file in an enqueue script function to make any javascript file asynchronous.</li>
					<li>Example: http://yoursite.com/features.js#admin</li>
					<li>This plugin includes a function that hooks into WordPress's "clean_url" hook. Every enqueue script can be affected by that hook.</li>
					<li>If the plugin is deactivated, the JS will continue to work since browsers treat # as a page anchor without any error message.</li>
				</ul>
			</li>
		</ul>
	</div>
	<?php

}
function frn_shortcode_report_field() {}
function plugin_options_report($input) {
	return $input;
}

?>