<?php

/*
Plugin Name: Slate Plugin
Plugin URI: https://github.com/christianstclair/slate-plugin
Description: A WordPress Admin theme
Author: christianstclair
Version: 1.0.70
Author URI: http://christianstclair.com/
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://github.com/christianstclair/slate-plugin',
    __FILE__,
    'slate-plugin'
);


// Enqueue Slate CSS and JS
function slate_files()
{
    wp_enqueue_style('slate-admin-theme', plugins_url('assets/css/slate.css', __FILE__), array( 'acf-input' ), '1.1.7');
    wp_enqueue_script('slate', plugins_url("assets/js/slate.js", __FILE__), array( 'jquery' ), '1.1.7');

    $pagetemplate = array( 'page_template' => basename(get_page_template()) );
    wp_localize_script('slate', 'template', $pagetemplate);
}


// Add a custom Site Title to the Dashboard Toolbar
function my_admin_bar()
{
    global $wp_admin_bar;
    $sitename = array(
        'id'     => 'site-name',                         // id of the existing child node (New > Post)
        'title'  => __(get_bloginfo('name'), 'textdomain'), // alter the title of existing node
        'parent' => false,                              // set parent to false to make it a top level (parent) node
        'meta' => array( 'target' => site_url() ), // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
    );

    //Add a link called 'My Link'...
    $wp_admin_bar->add_node($sitename);
    $wp_admin_bar->remove_menu('view-site');
    $wp_admin_bar->remove_menu('wp-logo-external');
}
add_action('wp_before_admin_bar_render', 'my_admin_bar', 999);



// Hide editor on specific pages.
function hide_editor()
{
  // Get the Post ID.
    if (! isset($_GET['post'])) {
        return;
    }
    $post_id = $_GET['post'];
    if (!isset($post_id)) {
        return;
    }
  // Hide the editor on a page with a specific page template
  // Get the name of the Page Template file.
    $template_file = get_post_meta($post_id, '_wp_page_template', true);
    if ($template_file == 'template-home.php') { // the filename of the page template
        remove_post_type_support('page', 'editor');
    }
}



// Actions
add_action('admin_head', 'hide_editor');
add_action('admin_enqueue_scripts', 'slate_files');
add_action('login_enqueue_scripts', 'slate_files');

function slate_add_editor_styles()
{
    add_editor_style(plugins_url('assets/css/editor-style.css', __FILE__));
}
add_action('after_setup_theme', 'slate_add_editor_styles');


// Remove the hyphen before the post state
add_filter('display_post_states', 'slate_post_state');
function slate_post_state($post_states)
{
    if (!empty($post_states)) {
        $state_count = count($post_states);
        $i = 0;
        foreach ($post_states as $state) {
            ++$i;
            ( $i == $state_count ) ? $sep = '' : $sep = '';
            echo "<span class='post-state'>$state$sep</span>";
        }
    }
}
